import numpy as np
from scipy import special

class SnakeNetwork():
    
    class WeightLayer():
        def __init__(self, input_nodes, output_nodes):
            self.weights = np.random.normal(-2.0, 2.0, (output_nodes, input_nodes))# pow(output_nodes, -0.5)

        def set_weights(self, weights):
            self.weights = weights

    def __init__(self, layers=(16,16,4), initialize=True):
        self.net = []
        if initialize:
            for w_index in range(1, len(layers)):
                inter_layer_weights = self.WeightLayer(layers[w_index-1], layers[w_index])
                self.net.append(inter_layer_weights)

    def query(self, input):
        np_input = np.array(input, ndmin=2).T
        for layer in self.net:
            np_input = self._sigmoid(np.dot(layer.weights ,np_input))            
        #print(np_input)
        return np_input

    def _sigmoid(self, x):
        return special.expit(x)