from snake import *
from snakenetwork import SnakeNetwork
from geneticalgorithm import GeneticAlgorithm
import pickle
import sys

genetic_algorithm = GeneticAlgorithm()
specimen_type = (12,32,4)

file = sys.argv[1]

snakes = []
if file.split('.')[-1] == 'snek':
    with (open(file, "rb")) as openfile:
        snakes.append(pickle.load(openfile))
        openfile.close()
elif file.split('.')[-1] == 'sneks':
    with (open(file, "rb")) as openfile:
        while True:
            try:
                snakes.append(pickle.load(openfile))
            except EOFError:
                break
    openfile.close()

for snake in snakes:
    game = SnakeGame(True)
    #game.playHuman(.4)
    game.playAIGUI(-1, snake)