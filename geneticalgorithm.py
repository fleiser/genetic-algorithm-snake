from snakenetwork import SnakeNetwork
from snake import *
from random import randint, uniform
import pickle
import numpy as np
import sys
from math import floor

class GeneticAlgorithm():
    def __init__(self):
        pass

    def create_population(self, specimen_type=(16,32,4), population_size=1000):
        population = []
        for i in range(0, population_size):
            net = SnakeNetwork(specimen_type)
            population.append(net)
        return population

    def score_population(self, population, max_turns=2000, number_of_tests=3, gui=False, speed=0.075, debug=False):
        if debug:
            print('Scoring population')
        population_score = []
        for index, specimen in enumerate(population):
            avg_score, avg_turns = 0, 0
            for test in range(0, number_of_tests):
                game_instance = SnakeGame(gui)
                if gui:
                    score = game_instance.playAIGUI(max_turns, specimen, speed)
                else:
                    score = game_instance.playAI(max_turns, specimen)
                avg_score += score[1]
                avg_turns += score[0]
            avg_score /= number_of_tests
            avg_turns /= number_of_tests

            population_score.append((index, self.fitness(avg_turns, avg_score)))
        #print(population_score)
        return population_score

    def score_individual(self, individual, max_turns, gui=False, speed=0.075, number_of_tests=1):
        game_instance = SnakeGame(gui)
        avg_score, avg_turns = 0, 0
        for test in range(0, number_of_tests):
            game_instance = SnakeGame(gui)
            if gui:
                score = game_instance.playAIGUI(max_turns, individual, speed)
            else:
                score = game_instance.playAI(max_turns, individual)
            avg_score += score[1]
            avg_turns += score[0]
        avg_score /= number_of_tests
        avg_turns /= number_of_tests
        return self.fitness(avg_turns, avg_score)
    
    def fitness(self, turns_survived, food_eaten):
        #return food_eaten**2+0.01*turns_survived
        #return food_eaten
        return food_eaten+0.01*turns_survived
        #return turns_survived

    def create_new_generation(self, population, population_mutation_rate=0.3, mutation_rate=0.15,
         selection_rate=.1, final_population_size=1000, max_turns = 2000, specimen_type=(16,32,4),
              save=True, save_score=15.0, debug=False, crossover='flip'):

        if debug:
            print('New Generation')
        new_generation = []
        selected_population = self.selection(population, self.score_population(population,max_turns), floor(final_population_size*selection_rate), debug)
        new_generation.extend(selected_population)
        
        if debug:
            print('Scoring selected population of', len(selected_population))
        #score new population
        score = self.score_population(new_generation, max_turns)
        biggest = max(score, key=lambda x: x[1])[1]
        smallest = min(score, key=lambda x: x[1])[1]
        scaled_score = []
        for specimen in score:
            #todo bad fix for divison by zero
            try:
                scaled_score.append((specimen[0],(specimen[1] - smallest)/(biggest - smallest)))
            except ZeroDivisionError:
                scaled_score.append((specimen[0], 0))
                
            if specimen[1] >= save_score and save:
                print('Saving snake', specimen[1])
                
                self.save_snake(new_generation[specimen[0]], 'individuals/'+str(specimen[1])+'!'+str(specimen_type)+'.snek')

        if debug:
            print('Crossover')
        while len(new_generation) < final_population_size:
            for specimen_one in scaled_score:
                if uniform(0.0, 1.0) < specimen_one[1]:
                    for specimen_two in scaled_score:
                        if uniform(0.0, 1.0) < specimen_two[1]:
                            if crossover == 'flip':
                                offsprings = self.crossover_flip(new_generation[specimen_one[0]], new_generation[specimen_two[0]], specimen_type)
                            elif crossover == '2point':
                                offsprings = self.crossover(new_generation[specimen_one[0]], new_generation[specimen_two[0]], specimen_type)
                            for offspring in offsprings:
                                if len(new_generation) >= final_population_size:
                                    break
                                new_generation.append(offspring)
      
        if debug:
            print('Mutations')
        for index, specimen in enumerate(new_generation):
            if uniform(0.0,1.0) < population_mutation_rate:
                new_generation[index] = self.mutate(specimen, mutation_rate)[0]

        if debug:
            print('New generation finished')
        return new_generation

    def selection(self, population, score, final_population_size = 500, debug=False):
        if debug:
            print('Selection in progress, #of', final_population_size)
        score_sorted = sorted(score, key=lambda x: x[1], reverse=True)
        selected = []
        
        scaled_score = []
        smallest, biggest = sys.maxsize, 0
        average = 0
        for specimen in score_sorted:
            if specimen[1] < smallest:
                smallest = specimen[1]
            if specimen[1] > biggest:
                biggest = specimen[1]
            average += specimen[1]
        average = average/len(score)

        print(smallest, biggest, average)
        for specimen in score:
            scaled_score.append((specimen[0],(specimen[1] - smallest)/(biggest - smallest)))

        while len(selected) < final_population_size:
            for specimen in scaled_score:
                if uniform(0.0, 1.0) < specimen[1]:
                    selected.append(population[specimen[0]])
                    if len(selected) == final_population_size:
                        break

        return selected

    def crossover(self, specimen_one, specimen_two, specimen_type=(16,32,4)):
        flat_one = specimen_one.net[0].weights.flatten()
        flat_two = specimen_two.net[0].weights.flatten()
        
        for layer in specimen_one.net[1:]:
            flat_one = np.hstack((flat_one,layer.weights.flatten()))         
        for layer in specimen_two.net[1:]:
            flat_two = np.hstack((flat_two,layer.weights.flatten()))        

        index_crossover_end = randint(0, len(flat_one))
        index_crossover_start = randint(0, index_crossover_end)
        flat_temp = flat_one
        flat_one[index_crossover_start:index_crossover_end] = flat_two[index_crossover_start:index_crossover_end]
        flat_two[index_crossover_start:index_crossover_end] = flat_temp[index_crossover_start:index_crossover_end]

        reshaped_one = SnakeNetwork(initialize=False)
        reshaped_two = SnakeNetwork(initialize=False)
        last_index = 0
        for index in range(0, len(specimen_type)-1):
            output_size = specimen_type[index+1]
            input_size = specimen_type[index]

            split_index = last_index+(output_size*input_size)
            
            split_array_one = flat_one[last_index:split_index]
            split_array_two = flat_two[last_index:split_index]
            reshaped_weights_one = SnakeNetwork.WeightLayer(1,1)
            reshaped_weights_two = SnakeNetwork.WeightLayer(1,1)
            reshaped_weights_one.set_weights(split_array_one.reshape(output_size, input_size))
            reshaped_weights_two.set_weights(split_array_two.reshape(output_size, input_size))
            
            reshaped_one.net.append(reshaped_weights_one)
            reshaped_two.net.append(reshaped_weights_two)
            
            last_index = split_index
        return (reshaped_one, reshaped_two)

    def crossover_flip(self, specimen_one, specimen_two, specimen_type=(16,32,4), flip_rate=0.05):
        flat_one = specimen_one.net[0].weights.flatten()
        flat_two = specimen_two.net[0].weights.flatten()
        
        for layer in specimen_one.net[1:]:
            flat_one = np.hstack((flat_one,layer.weights.flatten()))         
        for layer in specimen_two.net[1:]:
            flat_two = np.hstack((flat_two,layer.weights.flatten()))        

        for index, weight in enumerate(flat_one):
            if uniform(0.0, 1.0) < flip_rate:
                temp = weight
                flat_one[index] = flat_two[index]
                flat_two[index] = temp

        reshaped_one = SnakeNetwork(initialize=False)
        reshaped_two = SnakeNetwork(initialize=False)
        last_index = 0
        for index in range(0, len(specimen_type)-1):
            output_size = specimen_type[index+1]
            input_size = specimen_type[index]

            split_index = last_index+(output_size*input_size)
            
            split_array_one = flat_one[last_index:split_index]
            split_array_two = flat_two[last_index:split_index]
            reshaped_weights_one = SnakeNetwork.WeightLayer(1,1)
            reshaped_weights_two = SnakeNetwork.WeightLayer(1,1)
            reshaped_weights_one.set_weights(split_array_one.reshape(output_size, input_size))
            reshaped_weights_two.set_weights(split_array_two.reshape(output_size, input_size))
            
            reshaped_one.net.append(reshaped_weights_one)
            reshaped_two.net.append(reshaped_weights_two)
            
            last_index = split_index
        return (reshaped_one, reshaped_two)

    def mutate(self, specimen, mutation_rate):
        specimen_copy = specimen
        mutation_size = 0
        for network_layer in specimen_copy.net:
            for i, weights in enumerate(network_layer.weights):
                for j, val in enumerate(weights):
                    if uniform(0.0,1.0) < mutation_rate:                    
                        network_layer.weights[i, j] = np.random.normal(-2.0, 2.0)
                        mutation_size += 1
        return specimen_copy, mutation_size

    def save_population(self, population, file):
        with open(file, 'wb') as out:
            for specimen in population:
                pickle.dump(specimen, out, pickle.HIGHEST_PROTOCOL)

    def save_snake(self, snake, file):
        with open(file, 'wb') as out:
            pickle.dump(snake, out, pickle.HIGHEST_PROTOCOL)
            out.close()

    def load_population(self, file):
        snakes = []
        with (open(file, "rb")) as openfile:
            while True:
                try:
                    snakes.append(pickle.load(openfile))
                except EOFError:
                    break
        openfile.close()
        return snakes