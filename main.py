from snake import *
from snakenetwork import SnakeNetwork
from geneticalgorithm import GeneticAlgorithm


genetic_algorithm = GeneticAlgorithm()

specimen_type = (16,32,4)
initial_population=1000
final_population=1000
max_turns = -1
population_mutation_rate = .3
mutation_rate = .3
selection_rate = .2
generations = 100
save_score = 20.0
crossover = '2point'
debug = False
#game = SnakeGame(True)
#game.playHuman(.3)thegrandtour/comments/blyw7g/bbc_news_have_been_taking_notes_from_jezza/
load_existing = True


if load_existing:
    population = genetic_algorithm.load_population(
        'generations/remarkable/out_17.990000000000002!14.08783333333334!(16, 32, 4)!1000!1000!0.5flip.sneks')
    missing = initial_population - len(population)
    population.extend(genetic_algorithm.create_population(specimen_type=specimen_type, population_size=missing))
else:
    population = genetic_algorithm.create_population(population_size=initial_population, specimen_type=specimen_type)

previous_generation = population
for i in range(0,generations):
    print("gen",i)
    gen = genetic_algorithm.create_new_generation(population=previous_generation,selection_rate=selection_rate,
        final_population_size=final_population, mutation_rate=mutation_rate,
        population_mutation_rate=population_mutation_rate,save_score=save_score,
        specimen_type=specimen_type, debug=debug, crossover=crossover)
    previous_generation = gen

score = genetic_algorithm.score_population(population=previous_generation, gui=False, max_turns=max_turns)
score_sorted = sorted(score, key=lambda x: x[1], reverse=True)

best = []
avg_top = 0
top_score = 0
for score in score_sorted[:20]:
    best.append(previous_generation[score[0]])
    print(score)
    if top_score < score[1]:
        top_score = score[1]
    avg_top += score[1]
avg_top /= 20

genetic_algorithm.score_population(population=best, max_turns=max_turns,gui=True, number_of_tests=1)

data_out = (str(top_score)+', '+str(avg_top)+', '+str(specimen_type)+', '
            +str(initial_population)+', '+str(final_population)+', '+str(population_mutation_rate)+', '
            +str(mutation_rate)+', '+str(selection_rate)+', '+str(generations)+', '+str(max_turns)+crossover+'\n')

with open("data", "a") as myfile:
    myfile.write(data_out)
    myfile.close()

genetic_algorithm.save_population(best,'generations/out_'+str(top_score)+'!'+str(avg_top)+'!'
            +str(specimen_type)+'!'+str(initial_population)+'!'+str(final_population)+'!'
            +str(population_mutation_rate)+crossover+'.sneks')

genetic_algorithm.save_population(previous_generation, 'generations/out/'+str(avg_top)+str(specimen_type)+'.sneks')