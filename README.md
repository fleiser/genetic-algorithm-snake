# Optimizing a neural network to play a game of snake using a genetic algorithm #

In this project a neural network representing the snakes brain is optimized with a genetic algorithm and taught to play a game of snake.
The game itself is implemented using the PyGame library, the algorithm and neural network is written from scratch.

More information about the project: [Latex Report](report/snake_gen_alg.pdf)


![Good](vids/good.mp4)
![Snake example](vids/Peek 2019-05-07 15-39.mp4)