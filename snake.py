import pygame
import time
import sys
import numpy as np
from random import randint
from snakenetwork import SnakeNetwork

class Snake():
    def __init__(self, size):
        self.body = [(10,10),(10,9),(10,8)]
        self.direction = 'DOWN'
        self.speed = 20
        self.size = size
        self.energy = 200

    def set_direction(self, direction):
        if self.direction == 'UP' and direction == 'DOWN':
            return
        if self.direction == 'DOWN' and direction == 'UP':
            return
        if self.direction == 'LEFT' and direction == 'RIGHT':
            return
        if self.direction == 'RIGHT' and direction == 'LEFT':
            return
        self.direction = direction

    def draw_body(self, screen):
        for snake_part in self.body:
            pygame.draw.rect(screen, (0,255,0),(snake_part[0]*self.size, snake_part[1]*self.size, self.size,self.size))
        pygame.display.update()

    def move(self):
        last = self.body[0]
        if self.direction == 'DOWN':
            self.body[0] = (last[0], last[1]+1)
        if self.direction == 'UP':
            self.body[0] = (last[0], last[1]-1)
        if self.direction == 'LEFT':
            self.body[0] = (last[0]-1, last[1])
        if self.direction == 'RIGHT':
            self.body[0] = (last[0]+1, last[1])

        for index in range(1, len(self.body)):
            current = self.body[index]
            self.body[index] = last
            last = current
        self.energy -= 1
        #self.draw_body(screen)

    def consume(self):
        self.body.append(self.body[-1])
        self.energy += 150


class Field():
    def __init__(self, gui=True):
        self.board = (21,21)
        self.square_size = 40
        self.snake = Snake(self.square_size)
        self.food = None
        self.score = 0
        self.game_over = False
        if gui:
            #pygame.init()
            #self.font = pygame.font.SysFont('monospace',15)
            self.screen = pygame.display.set_mode((self.board[0]*self.square_size,self.board[1]*self.square_size)) 

    def spawn_food(self):
        if self.food is None:
            self.score += 1
            while(self.food is None or self.food in self.snake.body):
                self.food = (randint(0,self.board[0]-1), randint(0,self.board[1]-1))
    
    def draw_food(self):
        pygame.draw.rect(self.screen, (255,255,255),(self.food[0]*self.square_size,self.food[1]*self.square_size,self.square_size,self.square_size))

    def check_collisions(self):
        head = self.snake.body[0]

        if head[0] > self.board[0]-1 or head[1] > self.board[1]-1 or head[0] < 0 or head[1] < 0:
            self.game_over = True

        if head in self.snake.body[1:]:
            self.game_over = True
        
        if self.snake.energy <= 0:
            self.game_over = True

        if head == self.food:
            self.snake.consume()
            self.food = None

    def get_snake_perspective(self):
        head = self.snake.body[0]
        # walls
        left_boundary, upper_boundary, right_boundary,lower_boundary = (head[0], head[1], (self.board[0]-1)-head[0], (self.board[1]-1)-head[1]) 
        #normalize        
        left_boundary, upper_boundary, right_boundary, lower_boundary = 1-(left_boundary/self.board[0]),1-(upper_boundary/self.board[0]),1-(right_boundary/self.board[0]),1-(lower_boundary/self.board[0])
        #print('l', left_boundary,'u', upper_boundary,'r', right_boundary,'d', lower_boundary)

        # food
        left_food, up_food, right_food, down_food = (0,0,0,0)
        if self.food is not None:
            if head[0] == self.food[0]:
                if self.food[1] < head[1]:
                    up_food = 1
                    down_food = 0
                elif self.food[1] > head[1]:
                    down_food = 1
                    up_food = 0

            if head[1] == self.food[1]:                
                if self.food[0] < head[0]:
                    left_food = 1
                    right_food = 0
                elif self.food[0] > head[0]:
                    right_food = 1
                    left_food = 0

        #print('l', left_food,'u', up_food,'r', right_food,'d', down_food)
        left_up_food, up_right_food, right_down_food, down_left_food = 0, 0, 0, 0
        for i in range(0, self.board[0]):
            if (head[0]+i, head[1]+1) == self.food:
                right_down_food = 1
                break
            elif (head[0]+i, head[1]-i) == self.food:
                up_right_food = 1
                break
            elif (head[0]-i, head[1]+i) == self.food:
                down_left_food = 1
                break
            elif (head[0]-i, head[1]-i) == self.food:
                left_up_food = 1
                break
        #print('lu', left_up_food,'ur', up_right_food,'rd', right_down_food,'dl', down_left_food)
        

        #itself
        left_self, up_self, right_self, down_self = (0,0,0,0)
        for column in range(0, head[0]):
            if (column, head[1]) in self.snake.body[1:]:
                #print("snake to the left")
                left_self = 1-((head[0] - column)/self.board[0])
                break
        for column in range(head[0], self.board[0]):
            if (column, head[1]) in self.snake.body[1:]:
                #print("snake to the right")
                right_self = 1-((column - head[0])/self.board[0])
                break
        for row in range(0, head[1]):
            if (head[0], row) in self.snake.body[1:]:
                #print("snake to the top")
                up_self = 1-((head[1] - row)/self.board[0])
                break
        for row in range(head[1], self.board[0]):
            if (head[0], row) in self.snake.body[1:]:
                #print("snake to the bottom")
                down_self = 1-((row - head[1])/self.board[0])
                break
        #print('l',left_self,'u', up_self,'r', right_self,'d', down_self)
        values = (left_boundary, upper_boundary, right_boundary,lower_boundary, left_self, up_self,\
             right_self, down_self, left_food, up_food, right_food, down_food, \
                 left_up_food, up_right_food, right_down_food, down_left_food)
        #print(values)
        return values

    #def draw_text(self, *text):
    #    for index, value in enumerate(text):
    #        self.screen.blit(self.font.render(value,1,(255,255,0)),(5,5+15*index))

class SnakeGame():
    def __init__(self, gui):
        self.field = Field(gui=gui)

    def playHuman(self, speed=.15):
        played_turns = 0
        while (True):
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return (played_turns, self.field.score)
            time.sleep(speed)
            #clear screen
            self.field.screen.fill((0,0,0))
            #do snake things
            keys_pressed = pygame.key.get_pressed()
            if keys_pressed[pygame.K_LEFT]:
                self.field.snake.set_direction('LEFT')
            if keys_pressed[pygame.K_RIGHT]:
                self.field.snake.set_direction('RIGHT')
            if keys_pressed[pygame.K_UP]:
                self.field.snake.set_direction('UP')
            if keys_pressed[pygame.K_DOWN]:
                self.field.snake.set_direction('DOWN')

            self.field.snake.move()
            self.field.snake.draw_body(self.field.screen)
            self.field.check_collisions()
            self.field.spawn_food()
            self.field.draw_food()
            self.field.get_snake_perspective()
            if self.field.game_over:
                pygame.quit()
                return (played_turns, self.field.score)
            played_turns += 1
            pygame.display.update()

    def playAIGUI(self, turns, neural_network, speed=0.1):
        played_turns = 0
        if turns == -1:
            while True:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        return (played_turns, self.field.score)
                #clear screen
                time.sleep(speed)
                self.field.screen.fill((0,0,0))
                #do snake things
                snake_vision = self.field.get_snake_perspective()
                output = neural_network.query(snake_vision)
                largest_index = max(range(len(output)), key = lambda x: output[x])
                if largest_index == 0:
                    self.field.snake.set_direction('LEFT')
                elif largest_index == 1:
                    self.field.snake.set_direction('RIGHT')                
                elif largest_index == 2:
                    self.field.snake.set_direction('UP')
                elif largest_index == 3:
                    self.field.snake.set_direction('DOWN')
                self.field.snake.move()
                self.field.snake.draw_body(self.field.screen)
                self.field.check_collisions()
                self.field.spawn_food()
                self.field.draw_food()
                if self.field.game_over:
                    pygame.quit()
                    return (played_turns, self.field.score)
                played_turns += 1
                pygame.display.update()
        else:
            while played_turns < turns:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        return (played_turns, self.field.score)
                #clear screen
                time.sleep(speed)
                self.field.screen.fill((0,0,0))
                #do snake things
                snake_vision = self.field.get_snake_perspective()
                output = neural_network.query(snake_vision)
                largest_index = max(range(len(output)), key = lambda x: output[x])
                if largest_index == 0:
                    self.field.snake.set_direction('LEFT')
                elif largest_index == 1:
                    self.field.snake.set_direction('RIGHT')                
                elif largest_index == 2:
                    self.field.snake.set_direction('UP')
                elif largest_index == 3:
                    self.field.snake.set_direction('DOWN')
                self.field.snake.move()
                self.field.snake.draw_body(self.field.screen)
                self.field.check_collisions()
                self.field.spawn_food()
                self.field.draw_food()
                if self.field.game_over:
                    pygame.quit()
                    return (played_turns, self.field.score)
                played_turns += 1
                pygame.display.update()

        return (played_turns, self.field.score)

    def playAI(self, turns, neural_network):
        played_turns = 0
        if turns == -1:
            while True:
            #for turn in range(0,turns):
                #do snake things
                output = neural_network.query(self.field.get_snake_perspective())
                largest_index = max(range(len(output)), key = lambda x: output[x])
                if largest_index == 0:
                    self.field.snake.set_direction('LEFT')
                elif largest_index == 1:
                    self.field.snake.set_direction('RIGHT')                
                elif largest_index == 2:
                    self.field.snake.set_direction('UP')
                elif largest_index == 3:
                    self.field.snake.set_direction('DOWN')
                self.field.snake.move()
                self.field.check_collisions()
                self.field.spawn_food()
                if self.field.game_over:
                    return (played_turns, self.field.score)
                played_turns += 1
        else:
            while played_turns < turns:
            #for turn in range(0,turns):
                #do snake things
                output = neural_network.query(self.field.get_snake_perspective())
                largest_index = max(range(len(output)), key = lambda x: output[x])
                if largest_index == 0:
                    self.field.snake.set_direction('LEFT')
                elif largest_index == 1:
                    self.field.snake.set_direction('RIGHT')                
                elif largest_index == 2:
                    self.field.snake.set_direction('UP')
                elif largest_index == 3:
                    self.field.snake.set_direction('DOWN')
                self.field.snake.move()
                self.field.check_collisions()
                self.field.spawn_food()
                if self.field.game_over:
                    return (played_turns, self.field.score)
                played_turns += 1
        return (played_turns, self.field.score)

